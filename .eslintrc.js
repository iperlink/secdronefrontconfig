// TODO: Linter still shows some errors in imports
module.exports = {
  "extends": "airbnb-base",
  "parser": "babel-eslint",
  "env": {
    "browser": true,
    "commonjs": true,
    "es6": true,
    "node": true,
  },
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true,
      "modules": true,
    },
    "sourceType": "module",
  },
  "plugins": [
    "react",
    "no-unused-expressions",
  ],
  "rules": {
    "strict": 0,
    "no-const-assign": "warn",
    "no-console": 0,
    "no-this-before-super": "warn",
    "no-undef": "warn",
    "no-unreachable": "warn",
    "no-unused-vars": "warn",
    "constructor-super": "warn",
    "valid-typeof": "warn",
    "arrow-parens": [2, "as-needed"],
    "semi": [2, "never"],
    "no-nested-ternary": [0, "never"],
    "no-underscore-dangle": [2, { "allow": ["_id", "_ids", "_events", "_d"] }],
    "no-throw-literal": 0,
    "no-confusing-arrow": 0,
    "react/jsx-uses-vars": ["error"],
    "react/jsx-uses-react": 2,
    "no-unused-expressions": 0,
    "chai-friendly/no-unused-expressions": 0,
    "class-methods-use-this": 0,
    "import/prefer-default-export": 0,
    "global-require": 0,
    "import/no-extraneous-dependencies": ["error", {devDependencies:true, "packageDir": "./" }],
    "max-len": ["error", { "code": 100, "ignoreStrings": true }]
  },
  "globals": {
    "describe": true,
    "beforeEach": true,
    "afterEach": true,
    "before": true,
    "it": true,
    "test": true,
    "after": true,
    "vex": true,
  },
  "settings": {
    "import/resolver": {
      "webpack": {
        "config": __dirname + "/webpack.make.js"
      },
    },
  },
}
