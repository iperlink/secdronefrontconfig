import { dataSources } from './dataSources'

import { mutationsArray } from './mutations'

import { flexArray } from './flex'
import { registerForms } from './register'
import QUERYS from './querys'
import { routeArr } from './routes'
const api = 'api.wason.cloud'
const formArray = {
  ...mutationsArray,
  ...registerForms,
}
export const config = {
  api,
  dataSources,
  formArray,
  QUERYS,
  routeArr,
  registerForms,
  flexArray,
}


