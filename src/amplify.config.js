export default {
  apiGateway: {
    REGION: ' us-east-1',
    URL: 'https://tryzwv06r6.execute-api.us-east-1.amazonaws.com/dev',
  },
  cognito: {
    REGION: ' us-east-1',
    USER_POOL_ID: 'us-east-1_s7qqPxTRo',
    APP_CLIENT_ID: '7nsaukgik3kb5vb7ko3o3nti63',
    // IDENTITY_POOL_ID: 'YOUR_IDENTITY_POOL_ID',
  },
}
