import gql from 'graphql-tag'

export const setSelectedNameMutation = gql`
  mutation setSelectedName($selectedName: String) {
    setSelectedName(selectedName: $selectedName) @client
  }
`