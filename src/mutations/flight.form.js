export const createFlightForm = {
  mainMut: 'createFlight',
  title: 'Crear vuelo',
  file: 'flight.form.js',
  updateQuerys: ['flights'],
  fields: [
    {
      name: 'name',
      type: 'text',
      config: {
        placeholder: 'Ingrese nombre...',
        label: 'Nombre',
      },
    },
    {
      type: 'select',
      name: 'planningId',
      config: {
        multi: false,
        label: 'planificacion',
        id: 'planningId',
        dataFrom: {
          Q: 'readyPlannings',
          value: '_id',
          label: 'identifier',
        },
      },
    },
    {
      name: 'date',
      type: 'date',
      config: {
        placeholder: '2018-01-20',
        label: 'Fecha',
      },
      onChange: null,
    },
    {
      name: 'time',
      type: 'time',
      config: {
        label: 'Hora',
      },
      onChange: null,
    },
    {
      name: 'notify',
      type: 'text',
      config: {
        label: 'Compartir',
      },
    },
    {
      type: 'select',
      name: 'pilot',
      config: {
        multi: false,
        label: 'piloto',
        dataFrom: {
          Q: 'pilots',
          value: '_id',
          label: 'name',
        },
      },
    },
    {
      name: 'kml',
      type: 'text',
      config: {
        placeholder: 'Ingrese link de kml...',
        label: 'Kml',
      },
    },
    {
      name: 'images',
      type: 'file',
      config: {
        id: 'images',
        placeholder: 'Ingrese link de imagenes...',
        label: 'Imagenes',
      },
    },
    {
      name: 'report',
      type: 'file',
      config: {
        id: 'report',
        label: 'Reporte',
      },
    },
  ],
  create: true,
}
