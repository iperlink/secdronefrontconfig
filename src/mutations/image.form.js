export const createImageForm = {
  mainMut: 'createImage',
  file: 'image.form.js',
  updateQuerys: ['images'],
  fields: [
    {
      name: 'name',
      type: 'text',
      config: {
        placeholder: 'Ingrese nombre...',
        label: 'Nombre',
      },
      onChange: null,
    },
    {
      name: 'url',
      type: 'text',
      config: {
        placeholder: 'Ingrese url...',
        label: 'url',
      },
    },
    {
      name: 'tags',
      type: 'text',
      config: {
        placeholder: 'Ingrese tags...',
        label: 'tags',
      },
    },
  ],
  create: true,
}
