import gql from 'graphql-tag'
import _ from 'lodash'

import {
  sendValueToLocalState
} from '../utils/forms'

import QUERYS from '../querys'

const getOptionsFromQuery = (
  endpoint,
  query,
  container,
  value,
  label,
) => {
  return fetch(endpoint,
    {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query }),
    })
    .then((response) => {
      return response.json();
    }).then((json) => {
      const options = _.map(json.data[container], data => {
        return { value: data[value], label: data[label] }
      })
      return { options };
    })
}

const getOptions = (input, callback) => {
  return fetch(`http://localhost:4000/graphql`,
    {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `{points {
      _id
      name
    }}` }),
    })
    .then((response) => {
      return response.json()
    }).then((json) => {
      const options = _.map(json.data.points, data => {
        return { value: data._id, label: data.name }
      })
      return { options }
    })
}

export const formArray = {
  createUsersForm: {
    name: 'createUsersForm',
    updateQuery: {
      Q: QUERYS.Users,
      N: 'Users',
    },
    paramToUpdate: 'users',
    dataLoadQuerys: [{
      Q: QUERYS.Points,
      N: 'Points',
    }],
    listName: 'userList',
    fields: [
      {
        type: 'text',
        name: 'name',
        config: {
          placeholder: 'Insert name.',
          label: 'Nombre',
        },
        onChange: null,
      },
      {
        type: 'text',
        name: 'email',
        config: {
          placeholder: 'Insert email.',
          label: 'Email'
        },
        onChange: null,
      },
      {
        type: 'select',
        name: 'points',
        config: {
          multi: false,
          label: 'Puntos',
          dataFrom: { Q: 'Points', param: 'points', value: '_id', label: 'name' }
        },
        onChange: (val) => {
        },
      },
    ],
    create: true,
    onComplete: null,
    updateFn: null,
    mutation: {
      Q: gql`
        mutation createUserMutation($name: String!,$email: String!){
          createUser(email: $email, name: $name) {
            email
            name
          }
        }
    `,
      N: 'createUser'
    }
  },
  /*
    createPointsForm: {
      name: 'createPointsForm',
      updateQuery: {
        Q: QUERYS.Points,
        N: 'Points',
      },
      listName: 'pointList',
      fields: [
        {
          type: 'text',
          name:'name', 
          config: {
            placeholder: 'Insert name.',
            label: 'Nombre',
          },
          onChange: null,
        },
      ],
      create: true,
      mutation: {
        Q: gql`
          mutation AddPointsMutation($name: String!){
            addPoints( name: $name) {
              name
            }
          }
      `}
    },
  
    createVideosForm: {
      name: 'createVideosForm',
      updateQuery: {
        Q: QUERYS.Videos,
        N: 'Videos',
      },
      dataLoadQuerys: [{
        Q: QUERYS.Points,
        N: 'Points',
      }],
      listName: 'videosList',
      fields: [
        {
          type: 'text',
          name:'name', 
          config: {
            placeholder: 'Insert name.'
          },
          onChange: null,
        },
        {
          type: 'text',
          name:'url', 
          config: {
            placeholder: 'Insert url.'
          },
          onChange: null,
        },
        {
          type: 'asyncSelect',
          name:'points',
          config: {
            multi:false,
            loadOptions: getOptions,
        },
          onChange: null,
        },
      ],
      create: true,
      mutation: {
        Q: gql`
          mutation AddVideosMutation(
            $name: String!,
            $url: String!,
            $point_id: String!,
          ){
            addVideos(
              name: $name,
              url: $url,
              point_id: $point_id,
            ) {
              name
              url
              point_id
            }
          }
      `}
    }
    */
}
