export const createPilotForm = {
  file: 'createPilot.form.js',
  mainMut: 'createPilot',
  title: 'Crear piloto',
  updateQuerys: ['pilots'],
  fields: [
    {
      name: 'name',
      type: 'text',
      config: {
        placeholder: 'Ingrese nombre del piloto...',
        label: 'Nombre',
      },
    },
    {
      name: 'email',
      type: 'text',
      config: {
        placeholder: 'Ingrese email del piloto...',
        label: 'Email',
      },
    },
    {
      name: 'token',
      type: 'text',
      config: {
        placeholder: 'Ingrese token del piloto...',
        label: 'token',
      },
    },
    {
      type: 'select',
      name: 'group',
      config: {
        multi: false,
        label: 'grupo',
        dataFrom: {
          Q: 'groups',
          value: '_id',
          label: 'name',
        },
      },
    },
  ],
  create: true,
}
