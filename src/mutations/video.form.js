export const createVideoForm = {
  mainMut: 'createVideo',
  title: 'Crear video',
  file: 'video.form.js',
  updateQuerys: ['videos'],
  fields: [
    {
      name: 'name',
      type: 'text',
      config: {
        placeholder: 'Ingrese nombre...',
        label: 'Nombre',
      },
      onChange: null,
    },
    {
      type: 'select',
      name: 'flightId',
      config: {
        multi: false,
        label: 'vuelo',
        dataFrom: {
          Q: 'flights',
          value: '_id',
          label: 'identifier',
        },
      },
    },
    {
      name: 'url',
      type: 'text',
      config: {
        placeholder: 'Ingrese url...',
        label: 'url',
      },
    },
    {
      name: 'tags',
      type: 'text',
      config: {
        placeholder: 'Ingrese tags...',
        label: 'tags',
      },
    },
    {
      name: 'row',
      config: {
        multi: false,
        label: 'Categoria',
        options: [
          {
            label: 'Destacado',
            value: 'destacado',
          },
          {
            label: 'Normal',
            value: 'normal',
          },
        ],
      }
    }
  ],
  create: true,
}
