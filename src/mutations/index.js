import { createImageForm } from './image.form'
import { createPilotForm } from './pilot.form'
import {
  createPlanningForm,
} from './planning.form'
import { createPlanningTemplateForm } from './planningTemplate.form'
import { createPlanningTemplateSuperadminForm } from './planningTemplateSuperAdmin.form'
import { createFlightForm } from './flight.form'
import { createFlightAdminForm } from './fligthAdmin.form'
import { createVideoForm } from './video.form'

export const mutationsArray = {
  createFlightForm,
  createFlightAdminForm,
  createImageForm,
  createPilotForm,
  createPlanningForm,
  createPlanningTemplateForm,
  createPlanningTemplateSuperadminForm,
  createVideoForm,
}
