export const createPlanningForm = {
  mainMut: 'createPlanning',
  title: 'Crear planificacion',
  file: 'planning.form.js',
  updateQuerys: ['plannings', 'pendingPlannings'],
  paramToUpdate: 'plannings',
  listName: 'planningList',
  fields: [
    {
      type: 'select',
      name: 'templateId',
      edit: true,
      config: {
        multi: false,
        label: 'mision',
        dataFrom: {
          Q: 'planningTemplates',
          value: '_id',
          label: 'objective',
        },
      },
    },
    {
      name: 'dateSelected',
      type: 'date',
      config: {
        placeholder: '2018-01-20',
        label: 'Fecha',
      },
      onChange: null,
    },
    {
      name: 'time',
      type: 'time',
      edit: true,
      config: {
        label: 'Hora',
      },
      onChange: null,
    },
    {
      name: 'sector',
      type: 'text',
      config: {
        label: 'Sector',
      },
    },
    {
      name: 'tags',
      type: 'text',
      config: {
        placeholder: 'Ingrese Tags...',
        label: 'Tags',
      },
    },

  ],
  create: true,
}
