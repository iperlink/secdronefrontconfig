
export const createPlanningTemplateSuperadminForm = {
  mainMut: 'createPlanningTemplate',
  file: 'planningTemplate.form',
  title: 'Crear mision',
  updateQuerys: ['planningTemplates'],
  fields: [
    {
      name: 'area',
      type: 'text',
      config: {
        placeholder: 'Ingrese Area...',
        label: 'Area',
      },
      onChange: null,
    },
    {
      name: 'objective',
      type: 'text',
      config: {
        placeholder: 'Ingrese Objetivo...',
        label: 'Objetivo',
      },
      onChange: null,
    },
    {
      name: 'groundCoordinator',
      type: 'text',
      config: {
        placeholder: 'Ingrese Coordinador...',
        label: 'Coordinador',
      },
      onChange: null,
    },
    {
      name: 'frecuency',
      type: 'select',
      config: {
        multi: false,
        label: 'Frecuencia',
        options: [
          {
            label: 'N/A',
            value: 'n/a',
          },{
            label: 'Diaria',
            value: 'diaria',
          },
          {
            label: 'Tres veces por semana',
            value: 'Tres veces por semana',
          },
          {
            label: 'Semanal',
            value: 'Semanal',
          },
          {
            label: 'Quincenal',
            value: 'Quincenal',
          },
          {
            label: 'Mensual',
            value: 'Mensual',
          },
          {
            label: 'Trimestral',
            value: 'Trimestral',
          },
          {
            label: 'Semestral',
            value: 'Semestral',
          },
          {
            label: 'Anual',
            value: 'Anual',
          },
        ],
      },
    },
    {
      type: 'select',
      name: 'contract',
      config: {
        multi: false,
        label: 'contrato',
        dataFrom: {
          Q: 'contracts',
          value: '_id',
          label: 'name',
        },
      },
    },
  ],
  create: true,
}
