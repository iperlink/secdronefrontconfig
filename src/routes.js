const enalbleTesting = false

const routeArr = {
  master: {
    path: '/',
    component: 'videoList',
    title: 'home',
    link: 'home',
    args: {
      listName: 'videoList',
      loadingComp: 'Button',
      errorComp: 'Button',
      success: 'CardRow'
    }
  },

  childs: [
    {
      path: '/crearEmpresa',
      component: 'createCompanyForm',
      privileges: ['superAdmin'],
      group: 'Usuarios',
      title: 'Creacion de empresa',
      link: 'Creacion de empresa',
      args: {
        formName: 'createCompany'
      }
    },
    {
      path: '/resetDemo',
      component: 'resetDemoList',
      privileges: ['superAdmin'],
      group: 'Demo',
      title: 'resetDemo',
      link: 'resetDemo',
      args: {
        filtered: true,
        listName: 'resetDemoList',
        success: 'Table',
        filterKeys: ['email', 'name']
      }
    },
    {
      path: '/crearContrato',
      component: 'createContractForm',
      group: 'Usuarios',
      title: 'Creacion de contrato',
      link: 'Creacion de contrato',
      privileges: ['superAdmin'],
      args: {
        formName: 'createContract'
      }
    },
    {
      path: '/crearGrupo',
      component: 'createGroupForm',
      privileges: ['superAdmin'],
      group: 'Usuarios',
      title: 'Creacion de grupo',
      link: 'Creacion de grupo',
      args: {
        formName: 'createGroup'
      }
    },
    {
      path: '/crearUsuario',
      component: 'createUserForm',
      privileges: ['crear usuario', 'superAdmin'],
      group: 'Usuarios',
      title: 'Creacion de usuario',
      link: 'Creacion de usuario',
      args: {
        formName: 'createUser'
      }
    },

    {
      path: '/crearPiloto',
      component: 'createPilotForm',
      privileges: ['crear piloto', 'superAdmin'],
      group: 'Usuarios',
      title: 'Creacion de piloto',
      link: 'Creacion de piloto',
      args: {
        formName: 'createPilot'
      }
    },
    {
      path: '/empresas',
      component: 'companyList',
      privileges: ['superAdmin'],
      title: 'Empresas',
      link: 'Ver empresas',
      group: 'Usuarios',
      args: {
        listName: 'companyList',
        success: 'Table'
      }
    },
    {
      path: '/contratos',
      component: 'contractList',
      privileges: ['superAdmin'],
      title: 'Contratos',
      link: 'Ver contratos',
      group: 'Usuarios',
      args: {
        listName: 'contractList',
        filterKeys: ['name'],
        success: 'Table'
      }
    },
    {
      path: '/usuarios',
      component: 'userList',
      title: 'Usuarios',
      link: 'Ver usuarios',
      privileges: ['ver usuario'],
      group: 'Usuarios',
      args: {
        filtered: true,
        listName: 'userList',
        success: 'Table',
        filterKeys: ['email', 'name']
      }
    },
    {
      path: '/usuariossuperAdmin',
      component: 'userAdminList',
      title: 'Usuarios',
      link: 'Ver usuarios',
      privileges: ['superAdmin'],
      group: 'Usuarios',
      args: {
        filtered: true,
        listName: 'userAdminList',
        success: 'Table',
        filterKeys: ['contract', 'company']
      }
    },
    {
      path: '/grupos',
      component: 'groupList',
      title: 'Grupos',
      link: 'Ver grupos',
      privileges: ['ver grupos'],
      group: 'Usuarios',
      args: {
        listName: 'groupList',
        success: 'Table'
      }
    },
    {
      path: '/gruposSuperAdmin',
      component: 'groupAdminList',
      title: 'Grupos',
      link: 'Ver grupos',
      privileges: ['superAdmin'],
      group: 'Usuarios',
      args: {
        listName: 'groupAdminList',
        success: 'Table'
      }
    },
    {
      path: '/pilotos',
      component: 'pilotList',
      title: 'Pilotos',
      link: 'Ver pilotos',
      privileges: ['ver pilotos'],
      group: 'Usuarios',
      args: {
        listName: 'pilotList',
        success: 'Table'
      }
    },
    {
      path: '/pilotosSuperAdmin',
      component: 'pilotAdminList',
      title: 'Pilotos',
      link: 'Ver pilotos',
      privileges: ['superAdmin'],
      group: 'Usuarios',
      args: {
        listName: 'pilotAdminList',
        success: 'Table'
      }
    },
    {
      path: '/companyEdit/',
      component: 'companyEditList',
      privileges: ['', 'superAdmin'],
      title: 'Editar empresa',
      link: 'Editar empresa',
      group: 'Usuarios',
      args: {
        listName: 'companyEditList',
        success: 'Table'
      }
    },
    {
      path: '/contractEdit/',
      component: 'contractEditList',
      privileges: ['superAdmin'],
      title: 'Editar contrato',
      link: 'Editar contrato',
      group: 'Usuarios',
      args: {
        listName: 'contractEditList',
        success: 'Table'
      }
    },
    {
      path: '/userEdit/',
      component: 'userEditList',
      privileges: ['superAdmin'],
      title: 'Editar usuario',
      link: 'Editar usuario',
      group: 'Usuarios',
      args: {
        listName: 'userEditList',
        success: 'Table'
      }
    },
    {
      path: '/pilotEdit/',
      component: 'pilotEditList',
      privileges: ['superAdmin', 'admin'],
      title: 'Editar piloto',
      link: 'Editar piloto',
      group: 'Usuarios',
      args: {
        listName: 'pilotEditList',
        success: 'Table'
      }
    },
    {
      path: '/groupEdit/',
      component: 'groupEditList',
      privileges: ['superAdmin'],
      title: 'Editar grupo',
      link: 'Editar grupo',
      group: 'Usuarios',
      args: {
        listName: 'groupEditList',
        success: 'Table'
      }
    },
    {
      path: '/crearMision',
      component: 'createPlanningTemplateForm',
      group: 'Misiones',
      privileges: ['crear mision'],
      title: 'Mision',
      link: 'Creacion de mision',
      args: {
        formName: 'createPlanningTemplate'
      }
    },
    {
      path: '/crearMisionsuperadmin',
      component: 'createPlanningTemplateSuperadminForm',
      group: 'Misiones',
      privileges: ['superAdmin'],
      link: 'Creacion de mision',
      title: 'Mision',
      args: {
        formName: 'createPlanningTemplate'
      }
    },
    {
      path: '/misiones',
      component: 'planningTemplateList',
      title: 'Misiones',
      link: 'Ver misiones',
      privileges: ['ver misiones'],
      group: 'Misiones',
      args: {
        filtered: true,
        listName: 'planningTemplateList',
        success: 'Table',
        filterKeys: ['area', 'groundCoordinator', 'frecuency', 'objective', 'contract']
      }
    },
    {
      path: '/misionesSuperadmin',
      component: 'planningTemplateAdminList',
      title: 'Misiones',
      link: 'Ver Misiones',
      privileges: ['superAdmin'],
      group: 'Misiones',
      args: {
        filtered: true,
        listName: 'planningTemplateAdminList',
        success: 'Table',
        filterKeys: ['area', 'groundCoordinator', 'frecuency', 'objective', 'contract']
      }
    },
    {
      path: '/planningTemplateEdit/',
      component: 'planningTemplateEditList',
      privileges: ['superAdmin', 'admin'],
      title: 'Editar misión',
      link: 'Editar misión',
      group: 'Misiones',
      args: {
        listName: 'planningTemplateEditList',
        success: 'Table'
      }
    },
    {
      path: '/crearPlanificacion',
      component: 'createPlanningForm',
      group: 'Planificaciones',
      privileges: ['crear planificacion', 'superAdmin'],
      title: 'Creacion de planificacion',
      link: 'Creacion de planificacion',
      args: {
        formName: 'createPlanning'
      }
    },
    {
      path: '/planificaciones',
      component: 'planningList',
      title: 'Planificaciones',
      link: 'Ver Planificaciones',
      privileges: ['ver planificaciones'],
      group: 'Planificaciones',
      args: {
        filtered: true,
        selectable: false,
        dateRangeFilter: true,
        dateRangeFilterKey: 'dateSelected',
        listName: 'planningList',
        success: 'Table',
        filterKeys: ['area', 'objective', 'status', 'sector', 'groundCoordinator']
      }
    },
    {
      path: '/planificacionesSuperAdmin',
      component: 'planningAdminList',
      title: 'Planificaciones',
      link: 'Ver planificaciones',
      privileges: ['superAdmin'],
      group: 'Planificaciones',
      args: {
        filtered: true,
        selectable: false,
        dateRangeFilter: true,
        dateRangeFilterKey: 'dateSelected',
        listName: 'planningList',
        success: 'Table',
        filterKeys: ['area', 'objective', 'status', 'sector', 'groundCoordinator']
      }
    },
    {
      path: '/planificacionesPendientes',
      component: 'pendingPlanningList',
      title: 'Autorizar',
      link: 'Autorizar',
      privileges: ['autorizar planificaciones'],
      group: 'Planificaciones',
      args: {
        listName: 'pendingPlanningList',
        success: 'Table'
      }
    },
    {
      path: '/planificacionesPendientesSuperAdmin',
      component: 'pendingPlanningAdminList',
      title: 'Autorizar',
      link: 'Autorizar',
      privileges: ['superAdmin'],
      group: 'Planificaciones',
      args: {
        listName: 'pendingPlanningAdminList',
        success: 'Table'
      }
    },
    {
      path: '/planningEdit/',
      component: 'planningEditList',
      privileges: ['admin'],
      title: 'Editar planificación',
      link: 'Editar planificación',
      group: 'Planificaciones',
      args: {
        listName: 'planningEditList',
        success: 'Table'
      }
    },
    {
      path: '/planningEditAdmin/',
      component: 'planningEditAdminList',
      privileges: ['superAdmin'],
      title: 'Editar planificación',
      link: 'Editar planificación',
      group: 'Planificaciones',
      args: {
        listName: 'planningEditAdminList',
        success: 'Table'
      }
    },
    {
      path: '/vuelos',
      component: 'flightList',
      title: 'Vuelos',
      link: 'Ver vuelos',
      privileges: ['ver vuelos'],
      group: 'Vuelos',
      args: {
        filtered: true,
        dateRangeFilter: true,
        dateRangeFilterKey: 'date',
        listName: 'flightList',
        success: 'Table',
        filterKeys: ['area', 'objective', 'contract', 'pilot']
      }
    },

    {
      path: '/vuelosSuperAdmin',
      component: 'flightAdminList',
      title: 'Vuelos',
      link: 'Ver vuelos',
      privileges: ['superAdmin'],
      group: 'Vuelos',
      args: {
        filtered: true,
        dateRangeFilter: true,
        dateRangeFilterKey: 'date',
        listName: 'flightAdminList',
        success: 'Table',
        filterKeys: [
          'area',
          'objective',
          'contract',
          'pilot',
          'groundCoordinator',
          'sector',
          'objective'
        ]
      }
    },
    {
      path: '/flightEditList/',
      component: 'flightEditList',
      privileges: ['superAdmin'],
      title: 'Editar vuelos',
      link: 'Editar Vuelos',
      group: 'Vuelos',
      args: {
        listName: 'flightEditList',
        success: 'Table'
      }
    },
    {
      path: '/crearVuelo',
      component: 'createFlightForm',
      group: 'Vuelos',
      privileges: ['crear vuelo'],
      title: 'Creacion de vuelo',
      link: 'Creacion de vuelo',
      args: {
        formName: 'createFlight'
      }
    },
    {
      path: '/crearVueloSuperAdmin',
      component: 'createFlightAdminForm',
      group: 'Vuelos',
      privileges: ['superAdmin'],
      title: 'Creacion de vuelo',
      link: 'Creacion de vuelo',
      args: {
        formName: 'createFlight'
      }
    },
    {
      path: '/videos/',
      component: 'videoList',
      privileges: ['ver videos', 'superAdmin'],
      title: 'Videos',
      link: 'Ver videos',
      group: 'Videos',
      args: {
        listName: 'videoList',
        filtered: true,
        groupBy: 'row',
        filterKeys: ['area', 'groundCoordinator'],
        dateRangeFilter: true,
        dateRangeFilterKey: 'date',
        success: 'CardRow'
      }
    },
    {
      path: '/videolist/',
      component: 'videoList',
      privileges: ['superAdmin'],
      title: 'Categorias',
      link: 'Ver categorias',
      group: 'Videos',
      args: {
        listName: 'videoList',
        filtered: true,
        filterKeys: ['area', 'groundCoordinator'],
        dateRangeFilter: true,
        dateRangeFilterKey: 'date',
        success: 'Table'
      }
    },
    {
      path: '/videotag/',
      component: 'videoTag',
      privileges: ['superAdmin'],
      title: 'Etiquetas',
      link: 'Ver etiquetas',
      group: 'Videos',
      args: {
        listName: 'videoTag',
        success: 'Table'
      }
    },
    {
      path: '/videoedit/',
      component: 'videoEditList',
      privileges: ['superAdmin'],
      title: 'Editar video',
      link: 'Editar video',
      group: 'Videos',
      args: {
        listName: 'videoEditList',
        success: 'Table'
      }
    },
    {
      path: '/fligthvideos/:flight_id',
      component: 'videoList',
      privileges: ['ver videos', 'superAdmin'],
      title: 'Ver videos',
      group: 'hidden',
      args: {
        listName: 'videoList',
        filtered: true,
        groupBy: 'ubicacion',
        filterKeys: ['area', 'groundCoordinator'],
        dateRangeFilter: true,
        dateRangeFilterKey: 'date',
        success: 'CardRow'
      }
    },
    {
      path: '/images',
      component: 'imageList',
      privileges: ['ver imagenes', 'superAdmin'],
      title: 'Imagenes',
      link: 'Ver imagenes',
      group: 'hidden',
      args: {
        listName: 'imageList',
        filtered: true,
        groupBy: 'ubicacion',
        filterKeys: ['area', 'groundCoordinator'],
        dateRangeFilter: true,
        dateRangeFilterKey: 'date',
        success: 'GalleryRow'
      }
    },
    {
      path: '/fligthimages/:_id',
      component: 'imageList',
      privileges: ['ver imagenes', 'superAdmin'],
      title: '',
      group: 'hidden',
      args: {
        listName: 'imageList',
        success: 'GalleryRow'
      }
    },
    {
      path: '/fligthreport/:_id',
      component: 'imageList',
      privileges: ['ver reporte', 'superAdmin'],
      title: 'Ver reporte',
      group: 'hidden',
      args: {
        listName: 'imageList',
        success: 'Pdf'
      }
    },
    {
      path: '/kml/:fligth_id',
      component: 'imageList',
      privileges: ['ver reporte', 'superAdmin'],
      title: 'Ver reporte',
      group: 'Kml',
      args: {
        listName: 'imageList',
        success: 'Kml'
      }
    },
    {
      path: '/createvideos',
      component: 'createVideoForm',
      privileges: ['crear videos', 'superAdmin'],
      title: 'Crear video',
      link: 'Crear video',
      group: 'Videos',
      args: {
        formName: 'createVideo'
      }
    }
  ]
}
if (enalbleTesting) {
  routeArr.childs.push({
    path: '/Tester',
    component: 'userList',
    title: 'Testing',
    group: 'Testing',
    args: {
      listName: 'userList',
      loadingComp: 'Button',
      errorComp: 'Button',
      success: 'Tester',
      filterKeys: ['email', 'name']
    }
  })
}

export { routeArr }


