export const setSelectedName = switchData => {
  const {
    localState,
    rowData,
    rowIndex,
    switchEfect,
  } = switchData
  switchEfect({
    variables:
      { selectedName: rowIndex },
  })
}
