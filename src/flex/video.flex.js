import {
  dataSources
} from '../dataSources'

import {
  formArray,
} from '../mutations'

export const videoFlex = {
  rows: [
    [
      {
        component: 'videoList',
        args:{
          listName:'recomendedList',
          success:'CardRow',
        },
      },
    ],[
      {
        component: 'videoList',
        args:{
          listName:'videoList',
          success:'CardRow',
        },
      },
    ],

  ]
}
