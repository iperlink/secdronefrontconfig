const recursive = require('inquirer-recursive')
//const pluralize = require('pluralize')
const capitalize = require('capitalize')

module.exports = function (plop) {
  plop.setPrompt('recursive', recursive)
  plop.setHelper('capitalize', x => capitalize(x))

  plop.setGenerator('tester', {
    description: 'make schema and route',
    prompts: [
      {
      type: 'recursive',
      message: 'Add a property to the Schema?',
      name: 'props',
      prompts: [{
        type: 'input',
        name: 'name',
        message: 'Property name?',
        validate: value => {
          if ((/.+/).test(value)) { return true }
          return 'name is required'
        },
      }, {
        type: 'list',
        name: 'type',
        message: 'Property type?',
        choices: ['String', 'Number', 'Boolean'],
        default: 0,
      }, {
        type: 'confirm',
        name: 'required',
        message: 'Is required?',
        default: true,
      }],
  }],
  
  })


  plop.setGenerator('list', {
    description: 'make list and route',
    prompts: [
    {
      type: 'input',
      name: 'listName',
      message: 'listName name please'
    },
    {
      type: 'input',
      name: '_checkboxFn',
      message: '_checkboxFn name please'
    },
    {
      type: 'input',
      name: 'secondaryQuery',
      message: 'secondaryQuery please'
    },
    {
      type: 'input',
      name: 'secondaryQueryName',
      message: 'secondaryQueryName name please'
    },
    {
      type: 'input',
      name: 'selector',
      message: 'selector name please'
    },
    {
      type: 'recursive',
      name: 'head',
      message: 'add titles to list',
      prompts: [
        {
          type:'input',
          name: 'headTitle',
          message: 'enter row title',
        },{
          type:'input',
          name: 'headKey',
          message: 'enter row key',
        },
      ]
  },
  ],
    actions: [
    //   {
    //     type: 'modify',
    //     path: 'src/config/routes.js',
    //     pattern: /(\/\/INSERT NEW)/g,
    //     templateFile: 'plop-templates/route.hbs'
    // },
      {
        type: 'modify',
        path: 'lists/example.js',
        pattern: /(\/\/INSERT NEW)/g,
        templateFile: 'plop-templates/list.hbs'
    },
      {
        type: 'modify',
        path: 'lists/index.js',
        pattern: /(\/\/INSERT NEW)/g,
        templateFile: 'plop-templates/listIndex.hbs'
    },
  ]
});
};