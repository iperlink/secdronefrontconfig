export const objFromDir = path => {
  const files = fs.readdirSync(`./schema/${path}`)
  const mutationsObj = _.map(files, f => {
    if (f== 'index') {
      return null
    }
    const objs = require(`./${path}/${f}`)
    return objs
  })
  const reduced = _.reduce(mutationsObj, (acc, elem) => {

    return {
      ...acc,
      ...elem,
    }
  }, {})
  return reduced
}