import gql from 'graphql-tag'

export const pilotEditList = {
  mainData: {
    name: 'pilots',
    fields: [
      '_id',
      'company',
      'contract',
      'email',
      'name',
      'token',
    ],
  },
  title: 'pilotos',
  querys: [
    { Q: 'groups', as: 'groups' },
  ],
  head: [
    { title: 'Nombre', key: 'name' },
    { title: 'Email', key: 'email' },
    { title: 'Token', key: 'token' },
    { title: 'Contrato', key: 'contract' },
    { title: 'Empresa', key: 'company' },
    { title: 'Editar', key: '_edit' },
  ],
  extras: {
    _editFn: async (_id, data, client) => {
      const editPilot = gql`
        mutation editPilot(
          $_id: String!,
          $name: String!,
          $email: String!,
          $token: String!,
          $group: String!,

          ) {
            editPilot(
            _id: $_id,
            name:$name,
            email:$email,
            token:$token,
            group:$group,
            ) {
            _id
          }
        }`
      const variables = {
        ...data,
        _id,
      }
      const res = await client.mutate({
        mutation: editPilot,
        variables
      })
      alert('editado con exito')
      location.reload()
    },
    editParams: [
      { name: 'name', label: 'nombre' },
      { name: 'email', label: 'email' },
      { name: 'token', label: 'token' },
      { name: 'group', label: 'grupo', select: 'groups' },
    ],
  },
}