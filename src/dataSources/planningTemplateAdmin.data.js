export const planningTemplateAdminList = {
  mainData: {
    name: 'planningTemplates',
    fields: [
      '_id',
      'area',
      'aprovedBy',
      'createdBy',
      'frecuency',
      'groundCoordinator',
      'contract',
      'group',
      'objective',
      'name',
      'status',
    ],
  },
  title: 'Misiones',
  aditionalQuerys: [
    { Q: 'setSelectedNameMutation', as: 'switchEfect' },
  ],
  head: [
    { title: 'Area', key: 'area' },
    { title: 'Contrato', key: 'contract' },
    { title: 'Objetivo', key: 'objective' },
    { title: 'Frecuencia', key: 'frecuency' },
    { title: 'Coordinador', key: 'groundCoordinator' },
    { title: 'Grupo', key: 'group' },
  ],
}
