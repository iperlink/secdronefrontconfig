import { formatDates, renderAsLink } from './utils'


const renderVideos = data => {
  console.log('c', data.videoCount)
  if (data.videoCount != 0) {
    return `/fligthvideos/${data._id}`
  }
  return null
}

const renderImages = data => {
  if (data.images) {
    return `/fligthimages/${data._id}`
  }
  return null
}
const renderReport = data => {
  if (data.report) {
    return `https://s3.amazonaws.com/secdroneimages/${data.report}`
  }
  return null
}
export const flightAdminList = {
  mainData: {
    name: 'flights',
    fields: [
      'name',
      '_id',
      'planningTime',
      'date',
      'time',
      'area',
      'groundCoordinator',
      'identifier',
      'contract',
      'objective',
      'kml',
      'pilot',
      'sector',
      'images',
      'report',
      'videoCount'
    ],
  },
  title: 'vuelos',
  aditionalQuerys: [
    { Q: 'setSelectedNameMutation', as: 'switchEfect' },
  ],
  head: [
    { title: 'Fecha', key: 'formated_date', size: 65 },
    { title: 'Hora', key: 'time', size: 30 },

    { title: 'Objetivo', key: 'objective' },
    { title: 'area', key: 'area' },
    { title: 'sector', key: 'sector' },
    { title: 'Coordinador', key: 'groundCoordinator' },
    { title: 'Contrato', key: 'contract' },
    { title: 'Piloto', key: 'pilot' },
    { title: 'Imagenes', key: 'images', custom: '_link', size: 60 },
    { title: 'Ruta', key: 'kml', custom: '_link', size: 20 },
    { title: 'Report', key: 'report', custom: '_link', size: 40 },
    { title: 'Videos', key: 'videos', custom: '_link', size: 40 },
  ],
  mappingFn: param => {
    const dates = formatDates(param, ['date'])
    const videos = renderVideos(param)
    const images = renderImages(param)
    const report = renderReport(param)
    return {
      ...param,
      ...dates,
      videos,
      images,
      report,
    }
  },
}
