import { formatDates } from './utils'

export const planningList = {
  mainData: {
    name: 'plannings',
    fields: [
      '_id',
      'area',
      'createdBy',
      'createdAt',
      'contract',
      'dateSelected',
      'groundCoordinator',
      'aproved_by',
      'objective',
      'identifier',
      'sector',
      'status',
      'time',
      'tags',
      'identifier',
    ],
  },
  title: 'Planificaciones',
  head: [
    { title: 'Objetivo', key: 'objective' },
    { title: 'Area', key: 'area' },
    { title: 'Sector', key: 'sector' },
    { title: 'Hora', key: 'time' },
    { title: 'Fecha', key: 'formated_dateSelected' },
    { title: 'Status', key: 'status' },
    { title: 'Aprobado por', key: 'aproved_by' },
    { title: 'Coordinador', key: 'groundCoordinator' },
  ],
  mappingFn: param => {
    const dates = formatDates(param, ['dateSelected'])
    return {
      ...param,
      ...dates,
    }
  },
}
