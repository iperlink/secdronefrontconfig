import { companyList } from './company.data'
import { resetDemoList } from './resetDemo'
import { companyEditList } from './companyEdit.data'
import { contractList } from './contract.data'
import { contractEditList } from './contractEdit.data'
import { flightList } from './flight.data'
import { flightEditList } from './flightEdit.data'
import { flightAdminList } from './flightAdmin.data'
import { groupList } from './groups.data'
import { groupEditList } from './groupsEdit.data'
import { groupAdminList } from './groupsAdmin.data'
import { imageList } from './image.data'
import { pendingPlanningList } from './updatePlanning.data'
import { pendingPlanningAdminList } from './updatePlanningAdmin.data'
import { planningEditAdminList } from './planningEditAdmin.data'
import { pendingPlanningTemplateList } from './updatePlanningTemplate.data'
import { pilotList } from './pilot.data'
import { pilotEditList } from './pilotEdit.data'
import { pilotAdminList } from './pilotAdmin.data'
import { planningList } from './planning.data'
import { planningEditList } from './planningEdit.data'
import { planningAdminList } from './planningAdmin.data'
import { planningTemplateList } from './planningTemplate.data'
import { planningTemplateEditList } from './planningTemplateEdit.data'
import { planningTemplateAdminList } from './planningTemplateAdmin.data'
import { readyPlanningList } from './readyPlanning.data'
import { userList } from './user.data'
import { userEditList } from './userEdit.data'
import { userAdminList } from './userAdmin.data'
import { videoList } from './video.data'
import { videoEditList } from './videoEdit.data'
import { videoTag } from './videoEditTag.data'
export const dataSources = {
  resetDemoList,
  companyList,
  companyEditList,
  contractList,
  contractEditList,
  flightList,
  flightEditList,
  flightAdminList,
  groupList,
  groupAdminList,
  groupEditList,
  imageList,
  pendingPlanningList,
  pendingPlanningAdminList,
  pendingPlanningTemplateList,
  planningList,
  planningEditList,
  planningEditAdminList,
  planningAdminList,
  planningTemplateList,
  planningTemplateEditList,
  planningTemplateAdminList,
  readyPlanningList,
  pilotList,
  pilotEditList,
  pilotAdminList,
  userList,
  userEditList,
  userAdminList,
  videoList,
  videoEditList,
  videoTag,
}
