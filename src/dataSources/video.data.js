import gql from 'graphql-tag'


export const videoList = {
  mainData: {
    name: 'videos',
    fields: [
      '_id',
      'area',
      'date',
      'duration',
      'groundCoordinator',
      'name',
      'time',
      'flight_id',
      'contract',
      'objective',
      'url',
      'tags',
      'row',
      'thumb',
    ],
    params: ['flight_id'],
  },
  aditionalQuerys: [
    { Q: 'setSelectedNameMutation', as: 'switchEfect' },
  ],
  title: 'videos',
  head: [
    { title: 'nombre', key: 'name' },
    { title: 'Objetivo', key: 'objective' },
    { title: 'Contrato', key: 'contract' },
    { title: 'Fecha', key: 'date' },
    { title: 'Hora', key: 'time' },
    { title: 'Categoria', key: 'row' },
    { title: 'Editar', key: '_edit' },
  ],
  extras: {
    _editFn: async (id, data, client) => {
      const editVideoRow = gql`
        mutation editVideoRows($_id: String!, $row: String!) {
          editVideoRows(_id: $_id, row:$row) {
            _id
          }
        }`
      const res = await client.mutate({
        mutation: editVideoRow,
        variables: { _id: id, ...data }
      })
      alert('editado con exito')
      location.reload()
    },
    editParams: [{ name: 'row', label: 'categoría' }],
  },
  dataParams: {
    subtitle: 'area',
    text: 'area',
    thumb: 'thumb',
    url: 'url',
  },
}
