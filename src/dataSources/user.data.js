export const userList = {
  mainData: {
    name: 'users',
    fields: [
      '_id',
      'name',
      'lastName',
      'phone',
      'position',
      'email',
      'contract',
      'company',
    ],
  },
  title: 'Lista de usuarios',
  head: [
    { title: 'Nombre', key: 'name' },
    { title: 'Apellido', key: 'lastName' },
    { title: 'Telefono', key: 'phone' },
    { title: 'Email', key: 'email' },
  ],
}
