

export const companyList = {
  mainData: {
    name: 'companies',
    fields: [
      'name',
      '_id',
    ],
  },
  title: 'empresas',
  head: [
    { title: 'Nombre', key: 'name' },
  ],
}
