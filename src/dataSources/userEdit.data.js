import gql from 'graphql-tag'
export const userEditList = {
  mainData: {
    name: 'users',
    fields: [
      '_id',
      'name',
      'lastName',
      'phone',
      'position',
      'group',
      'email',
      'contract',
      'company',
    ],
  },
  title: 'Lista de usuarios',
  querys: [
    { Q: 'groups', as: 'groups' },
  ],
  head: [
    { title: 'Nombre', key: 'name' },
    { title: 'Apellido', key: 'lastName' },
    { title: 'Telefono', key: 'phone' },
    { title: 'Contrato', key: 'contract' },
    { title: 'Cargo', key: 'position' },
    { title: 'Grupo', key: 'group' },
    { title: 'Email', key: 'email' },
    { title: 'Editar', key: '_edit' },
  ],
  extras: {
    _editFn: async (_id, data, client) => {
      const editUser = gql`
        mutation editUser(
          $_id: String!,
          $name: String!,
          $lastName: String!,
          $email: String!,
          $position: String!,
          $group: String!,
          $phone: String!,

          ) {
            editUser(
            _id: $_id,
            name:$name,
            email:$email,
            lastName:$lastName,
            position:$position,
            group:$group,
            phone:$phone,
            ) {
            _id
          }
        }`
      const variables = {
        ...data,
        _id,
      }
      const res = await client.mutate({
        mutation: editUser,
        variables
      })
      alert('editado con exito')
      location.reload()
    },
    editParams: [
      { name: 'name', label: 'nombre' },
      { name: 'lastName', label: 'apellido' },
      { name: 'email', label: 'email' },
      { name: 'group', label: 'grupos', select: 'groups' },
      { name: 'position', label: 'cargo' },
      { name: 'phone', label: 'telefono' },
    ],
  },
}
