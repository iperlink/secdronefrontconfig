

export const pilotAdminList = {
  mainData: {
    name: 'pilots',
    fields: [
      'name',
      '_id',
      'token',
      'email',
      'contract',
      'company',
    ],
  },
  title: 'pilotos',
  aditionalQuerys: [
    { Q: 'setSelectedNameMutation', as: 'switchEfect' },
  ],
  head: [
    { title: 'Nombre', key: 'name' },
    { title: 'Email', key: 'email' },
    { title: 'Token', key: 'token' },
    { title: 'Contrato', key: 'contract' },
    { title: 'Empresa', key: 'company' },
  ],
}