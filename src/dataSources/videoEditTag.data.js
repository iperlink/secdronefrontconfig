import gql from 'graphql-tag'

const renderUrl = data => {
  const { url } = data
  if (url) {
    const parsed = JSON.parse(url)
    const link = `https://security-drone.wistia.com/medias/${parsed.link}`
    return link
  }
  return ''
}

export const videoTag = {
  mainData: {
    name: 'videoTags',
    fields: [
      '_id',
      'date',
      'name',
      'time',
      'objective',
      'url',
      'tags',
    ]
    ,
    params: ['flight_id'],
  },
  aditionalQuerys: [
    { Q: 'setSelectedNameMutation', as: 'switchEfect' },
  ],
  title: 'videoTags',
  head: [
    { title: 'nombre', key: 'name' },
    { title: 'Tags', key: 'tags' },
    { title: 'Url', key: 'url', custom: '_link', size: 20 },
    { title: 'Editar', key: '_edit' },
  ],
  extras: {

    _editFn: async (id, data, client) => {
      const aprovePlanning = gql`
        mutation editVideoTags($_id: String!, $tags: String!) {
          editVideoTags(_id: $_id, tags:$tags) {
            _id
          }
        }`
      const res = await client.mutate({
        mutation: aprovePlanning,
        variables: { _id: id, tags: data }
      })
      alert('editado con exito')
      location.reload()
    },
    editParams: [{ name: 'tags', label: 'tags' }
    ],
  },
  mappingFn: param => {
    const url = renderUrl(param)
    return {
      ...param,
      url,
    }
  },
}
