import { formatDates } from './utils'

export const planningAdminList = {
  mainData: {
    name: 'plannings',
    fields: [
      '_id',
      'area',
      'createdBy',
      'createdAt',
      'contract',
      'dateSelected',
      'groundCoordinator',
      'aproved_by',
      'sector',
      'objective',
      'identifier',
      'status',
      'time',
      'tags',
      'identifier',
    ],
  },
  title: 'Planificaciones',
  head: [
    { title: 'Objetivo', key: 'objective', size: 130 },
    { title: 'Area', key: 'area'  },
    { title: 'Sector', key: 'sector'  },
    { title: 'Hora', key: 'time', size: 30  },
    { title: 'Fecha', key: 'formated_dateSelected', size: 65  },
    { title: 'Contrato', key: 'contract' },
    { title: 'Status', key: 'status' },
    { title: 'Aprobado por', key: 'aproved_by' },
    { title: 'Coordinador', key: 'groundCoordinator' },
  ],
  mappingFn: param => {
    const dates = formatDates(param, ['dateSelected'])
    return {
      ...param,
      ...dates,
    }
  },
}
