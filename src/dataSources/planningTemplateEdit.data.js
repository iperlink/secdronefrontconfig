import gql from 'graphql-tag'
export const planningTemplateEditList = {
  mainData: {
    name: 'pendingPlanningTemplates',
    fields: [
      '_id',
      'area',
      'aprovedBy',
      'createdBy',
      'frecuency',
      'groundCoordinator',
      'contract',
      'group',
      'objective',
      'status',
    ],
  },
  querys: [
    { Q: 'contracts', as: 'contracts' },
  ],
  title: 'Misiones',
  head: [
    { title: 'Objetivo', key: 'objective', size: 130 },
    { title: 'Contrato', key: 'contract' },
    { title: 'Grupo', key: 'group' },
    { title: 'Area', key: 'area' },
    { title: 'Frecuencia', key: 'frecuency', size: 70 },
    { title: 'Coordinador', key: 'groundCoordinator' },
    { title: 'Editar', key: '_edit' },
  ],
  extras: {
    _editFn: async (_id, data, client) => {
      const editPlanningTemplate = gql`
        mutation editPlanningTemplate(
          $_id: String!,
          $objective: String!,
          $contract: String!,
          $frecuency: String!,
          $groundCoordinator: String!,
          $area: String!,

          ) {
            editPlanningTemplate(
            _id: $_id,
            objective:$objective,
            contract:$contract,
            frecuency:$frecuency,
            groundCoordinator: $groundCoordinator,
            area:$area,
            ) {
            _id
          }
        }`
      const variables = {
        ...data,
        _id,
      }
      const res = await client.mutate({
        mutation: editPlanningTemplate,
        variables
      })
      alert('editado con exito')
      location.reload()
    },
    editParams: [
      { name: 'objective', label: 'Objetivo' },
      { name: 'area', label: 'Area' },
      { name: 'frecuency', label: 'Frecuencia' },
      { name: 'contract', label: 'Contrato', select:'contracts' },
      { name: 'groundCoordinator', label: 'Coordinador' },
    ],
  },
}
