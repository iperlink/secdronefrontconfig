import gql from 'graphql-tag'
import { formatDates, renderAsLink } from './utils'
const renderVideos = data => {
  return `/fligthvideos/${data._id}`
}
const renderImages = data => {
  return `/fligthimages/${data._id}`
}
const renderReport = data => {
  return `/fligthreport/${data._id}`
}
export const flightEditList = {
  mainData: {
    name: 'flights',
    fields: [
      'name',
      '_id',
      'planningTime',
      'date',
      'time',
      'area',
      'groundCoordinator',
      'identifier',
      'objective',
      'contract',
      'kml',
      'pilot',
      'images',
      'report',
    ],
  },
  title: 'vuelos',
  querys: [
    { Q: 'pilots', as: 'pilots' },
  ],
  head: [
    { title: 'Fecha', key: 'formated_date', size: 30 },
    { title: 'Hora', key: 'planningTime', size: 30 },
    { title: 'Area', key: 'area' },
    { title: 'Sector', key: 'sector' },
    { title: 'Objetivo', key: 'objective' },
    { title: 'Coordinador', key: 'groundCoordinator', size: 130 },
    { title: 'Piloto', key: 'pilot', size: 130 },
    { title: 'Editar', key: '_edit' },
  ],
  mappingFn: param => {
    const dates = formatDates(param, ['date'])
    const videos = renderVideos(param)
    const images = renderImages(param)
    const report = renderReport(param)
    return {
      ...param,
      ...dates,
      videos,
      images,
      report,
    }
  },
  extras: {
    _editFn: async (_id, data, client) => {
      const editFlight = gql`
        mutation editFlight(
          $_id: String!,
          $name: String!,
          $date: String!,
          $time: String!,
          $pilot: String!,
          $kml: String!,
          $images: String!,
          $report: String!,

          ) {
            editFlight(
            _id: $_id,
            name:$name,
            date:$date,
            time:$time,
            pilot:$pilot,
            images:$images,
            kml:$kml,
            report:$report,
            ) {
            _id
          }
        }`
      const variables = {
        ...data,
        _id,
      }
      const res = await client.mutate({
        mutation: editFlight,
        variables
      })
      alert('editado con exito')
      // location.reload()
    },
    editParams: [
      { name: 'name', label: 'nombre' },
      { name: 'date', label: 'Fecha', date: true },
      { name: 'time', label: 'Hora' },
      { name: 'pilot', label: 'Piloto', select: 'pilots' },
      { name: 'kml', label: 'Ruta' },
      { name: 'images', label: 'imagenes', file: true },
      { name: 'report', label: 'report', file: true },
    ],
  },
}
