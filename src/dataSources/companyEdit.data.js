
import gql from 'graphql-tag'
export const companyEditList = {
  mainData: {
    name: 'companies',
    fields: [
      'name',
      '_id',
    ],
  },
  title: 'empresas',
  head: [
    { title: 'Nombre', key: 'name' },
    { title: 'Editar', key: '_edit' },
  ],
  extras: {
    _editFn: async (_id,data,client) => {
      const editCompany = gql`
        mutation editCompany($_id: String!, $name: String!) {
          editCompany(_id: $_id, name:$name) {
            _id
          }
        }`
    const variables = {
      ...data,
      _id,
    }
    const res = await client.mutate({
      mutation: editCompany,
      variables
    })
    alert('editado con exito')
    location.reload()
    },
    editParams: [{name:'name', label: 'nombre'}],
  },
}
