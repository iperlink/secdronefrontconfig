import { formatDates } from './utils'
import gql from 'graphql-tag'
import _ from 'lodash'

export const pendingPlanningList = {
  mainData: {
    name: 'pendingPlannings',
    fields: [
      '_id',
      'area',
      'createdBy',
      'dateSelected',
      'groundCoordinator',
      'objective',
      'identifier',
      'contract',
      'status',
      'time',
      'tags',
      'identifier',
    ],
  },
  extras: {
    _checkboxFn: {
      fn: switchData => {
      },
      submit: async (selected, client) => {
        const ids = _.map(selected, x => x._id).join(',')
        const aprovePlanning = gql`
          mutation aprovePlanning($_id: String!) {
            aprovePlanning(_id: $_id) {
              _id
            }
          }`
        const { data } = await client.mutate({
          mutation: aprovePlanning,
          variables: { _id: ids }
        })
        alert('Planificaciones autorizadas')
        location.reload()
      },
      name: 'autorizar'
    },
    _editFn: (data) => {
    },
    selector: 'one',
  },
  title: 'Planificaciones',
  aditionalQuerys: [
    { Q: 'setSelectedNameMutation', as: 'switchEfect' },
  ],
  head: [
    { title: 'Objetivo', key: 'objective', size: 130 },
    { title: 'Area', key: 'area' },
    { title: 'Hora', key: 'time' },
    { title: 'Fecha', key: 'formated_dateSelected', size: 65 },

    { title: 'Coordinador', key: 'groundCoordinator' },
    { title: 'Autorizar', key: '_checkbox' },

  ],
  mappingFn: param => {
    const dates = formatDates(param, ['dateSelected'])
    return {
      ...param,
      ...dates,
    }
  },
}
