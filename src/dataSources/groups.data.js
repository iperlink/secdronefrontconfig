export const groupList = {
  mainData: {
    name: 'groups',
    fields: ['name', 'privileges', 'company', 'contract', '_id']
  },
  title: 'grupos',
  aditionalQuerys: [{ Q: 'setSelectedNameMutation', as: 'switchEfect' }],
  head: [{ title: 'Nombre', key: 'name' }]
}
