const renderReport = data => {
  return `https://s3.amazonaws.com/secdroneimages/${data.report}`
}

export const reportList = {

  mainData: {
    name: 'flightImages',
    fields: [
      '_id',
      'area',
      'date',
      'groundCoordinator',
      'objective',
      'name',
      'report',
    ],
    params: ['_id'],
  },
  title: 'reporte',
  aditionalQuerys: [
    { Q: 'setSelectedNameMutation', as: 'switchEfect' },
  ],
  head: [
    { title: 'nombre', key: 'name' },
    { title: 'area', key: 'area' },
    { title: 'Coordinador', key: 'groundCoordinator' },
    { title: 'Fecha', key: 'date' },
    { title: 'Hora', key: 'time' },
    { title: 'Duracion', key: 'duration' },
    { title: 'Link', key: 'url' },
  ],
  mappingFn: param => {

    const report = renderReport(param)
    return {
      report,
    }
  },
  dataParams: {
    pdf: 'report'
  },
}
