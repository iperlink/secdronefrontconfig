const renderReport = data => {
  return `https://s3.amazonaws.com/secdroneimages/${data.report}`
}

export const imageList = {
  mainData: {
    name: 'flightImages',
    fields: [
      '_id',
      'area',
      'date',
      'groundCoordinator',
      'objective',
      'name',
      'images',
      'report',
    ],
    params: ['_id'],
  },
  title: 'imagenes',
  aditionalQuerys: [
    { Q: 'setSelectedNameMutation', as: 'switchEfect' },
  ],
  head: [
    { title: 'nombre', key: 'name' },
    { title: 'area', key: 'area' },
    { title: 'Coordinador', key: 'groundCoordinator' },
    { title: 'Fecha', key: 'date' },
    { title: 'Hora', key: 'time' },
    { title: 'Duracion', key: 'duration' },
    { title: 'Link', key: 'url' },
  ],
  dataParams: {
    subtitle: 'area',
    text: 'date',
    thumb: 'thumb',
    url: 'url',
  },

  mappingFn: param => {
    const report = renderReport(param)
    return {
      ...param,
      report,
    }
  },
  dataParams: {
    pdf: 'report'
  },
}
