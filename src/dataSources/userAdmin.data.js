export const userAdminList = {
  mainData: {
    name: 'users',
    fields: [
      '_id',
      'name',
      'lastName',
      'phone',
      'position',
      'group',
      'email',
      'contract',
      'company',
    ],
  },
  title: 'Lista de usuarios',

  head: [
    { title: 'Nombre', key: 'name' },
    { title: 'Apellido', key: 'lastName' },
    { title: 'Telefono', key: 'phone' },
    { title: 'Contrato', key: 'contract' },
    { title: 'Cargo', key: 'position' },
    { title: 'Grupo', key: 'group' },
    { title: 'Email', key: 'email' },
    { title: 'Empresa', key: 'company' },
  ],
  
}
