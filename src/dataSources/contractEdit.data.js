
import gql from 'graphql-tag'
export const contractEditList = {
  mainData: {
    name: 'contracts',
    fields: [
      'name',
      '_id',
      'company'
    ],
  },
  title: 'contratos',
  head: [
    { title: 'Nombre', key: 'name' },
    { title: 'Empresa', key: 'company' },
    { title: 'Editar', key: '_edit' },
  ],
  extras: {
    _editFn: async (_id,data,client) => {
      const editContract = gql`
        mutation editContract(
          $_id: String!,
          $name: String!,

          ) {
            editContract(
            _id: $_id,
            name:$name,
            ) {
            _id
          }
        }`
    const variables = {
      ...data,
      _id,
    }
    const res = await client.mutate({
      mutation: editContract,
      variables
    })
    alert('editado con exito')
    location.reload()
    },
    editParams: [{name:'name', label: 'nombre'}],
  },
}