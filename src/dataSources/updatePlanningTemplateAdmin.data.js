export const pendingPlanningTemplateAdminList = {
  mainData: {
    name: 'pendingPlanningTemplates',
    fields: [
      '_id',
      'area',
      'aprovedBy',
      'createdBy',
      'frecuency',
      'groundCoordinator',
      'contract',
      'group',
      'objective',
      'status',
    ],
  },
  extras: {
    _checkboxFn: {
      fn: switchData => {
      },
      submit: async (selected, client) => {
        const ids = _.map(selected, x => x._id).join(',')
        const aprovePlanningTemplate = gql`
          mutation aprovePlanningTemplate ($_id: String!) {
            aprovePlanning(_id: $_id) {
              _id
            }
          }`
        const { data } = await client.mutate({
          mutation: aprovePlanningTemplate,
          variables: { _id: ids }
        })
        alert('editado con exito')
        location.reload()
      },
      name: 'autorizar'
    },
    selector: 'all',
  },
  title: 'Misiones',
  aditionalQuerys: [
    { Q: 'setSelectedNameMutation', as: 'switchEfect' },
  ],
  head: [
    { title: 'Objetivo', key: 'objective', size: 130 },
    { title: 'Contrato', key: 'contract' },
    { title: 'Grupo', key: 'group' },
    { title: 'Area', key: 'area' },
    { title: 'Frecuencia', key: 'frecuency', size: 70 },
    { title: 'Coordinador', key: 'groundCoordinator' },
    { title: 'Autorizar', key: '_checkbox' },
  ],
}
