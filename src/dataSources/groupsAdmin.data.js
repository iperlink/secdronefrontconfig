export const groupAdminList = {
  mainData: {
    name: 'groups',
    fields: ['name', 'privileges', 'contract', 'company', '_id']
  },
  title: 'grupos',
  head: [
    { title: 'Nombre', key: 'name', size: 50 },
    { title: 'Contrato', key: 'contract', size: 60 },
    { title: 'Privilegios', key: 'privileges', size: 180 },
    { title: 'Empresa', key: 'company', size: 60 }
  ],
  mappingFn: param => {
    return {
      ...param
    }
  }
}
