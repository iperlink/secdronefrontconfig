

export const pilotList = {
  mainData: {
    name: 'pilots',
    fields: [
      'name',
      '_id',
      'token',
      'email',
      'contract',
    ],
  },
  title: 'pilotos',
  aditionalQuerys: [
    { Q: 'setSelectedNameMutation', as: 'switchEfect' },
  ],
  head: [
    { title: 'Nombre', key: 'name' },
    { title: 'Email', key: 'email' },
    { title: 'Token', key: 'token' },
  ],
}