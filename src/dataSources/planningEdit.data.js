import gql from 'graphql-tag'
import { formatDates } from './utils'
export const planningEditList = {
  mainData: {
    name: 'planningEdits',
    fields: [
      '_id',
      'area',
      'createdBy',
      'createdAt',
      'contract',
      'dateSelected',
      'groundCoordinator',
      'aproved_by',
      'sector',
      'objective',
      'identifier',
      'status',
      'time',
      'tags',
      'identifier'
    ]
  },
  querys: [{ Q: 'planningTemplates', as: 'planningTemplates' }],
  title: 'Planificaciones',
  head: [
    { title: 'Objetivo', key: 'objective', size: 130 },
    { title: 'Area', key: 'area' },
    { title: 'Sector', key: 'sector' },
    { title: 'Hora', key: 'time', size: 30 },
    { title: 'Fecha', key: 'formated_dateSelected', size: 65 },
    { title: 'Contrato', key: 'contract' },
    { title: 'Status', key: 'status' },
    { title: 'Aprobado por', key: 'aproved_by' },
    { title: 'Coordinador', key: 'groundCoordinator' },
    { title: 'Editar', key: '_edit' }
  ],
  mappingFn: param => {
    const dates = formatDates(param, ['dateSelected'])
    return {
      ...param,
      ...dates
    }
  },
  extras: {
    _editFn: async (_id, data, client) => {
      const editPlanning = gql`
        mutation editPlanning(
          $_id: String!
          $time: String!
          $dateSelected: String!
          $sector: String!
          $tags: String!
        ) {
          editPlanning(
            _id: $_id
            time: $time
            date_selected: $dateSelected
            sector: $sector
            tags: $tags
          ) {
            _id
          }
        }
      `
      const variables = {
        ...data,
        _id
      }
      console.log(data)
      const res = await client.mutate({
        mutation: editPlanning,
        variables
      })
      alert('editado con exito')
      location.reload()
    },
    editParams: [
      { name: 'time', label: 'Hora' },
      { name: 'dateSelected', label: 'Fecha', date: 'true' },
      { name: 'tags', label: 'Tags' },
      { name: 'sector', label: 'Sector' }
    ]
  }
}
