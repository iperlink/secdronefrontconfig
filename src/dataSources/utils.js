import moment from 'moment'

export const formatDates = (data, keys) => {
  const formated = _.reduce(keys, (acc, curr) => {
    if(curr){

      const param = data[curr]
      const mom = moment(param).add(4, 'hours')
      const result = {
        ...acc,
        [`formated_${curr}`]: mom.format('Do MMM YYYY'),
      }
      return result
    }
    return acc
  }, {})
  return {
    ...formated,
  }
}


