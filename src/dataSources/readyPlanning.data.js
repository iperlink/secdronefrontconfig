import { formatDates } from './utils'

export const readyPlanningList = {
  mainData: {
    name: 'readyPlannings',
    fields: [
      '_id',
      'area',
      'createdBy',
      'createdAt',
      'contract',
      'dateSelected',
      'groundCoordinator',
      'objective',
      'identifier',
      'status',
      'time',
      'tags',
      'identifier',
    ],
  },
  title: 'Planificaciones',
  aditionalQuerys: [
    { Q: 'setSelectedNameMutation', as: 'switchEfect' },
  ],
  head: [
    { title: 'Objetivo', key: 'objective' },
    { title: 'Area', key: 'area' },
    { title: 'Hora', key: 'time' },
    { title: 'Fecha', key: 'formated_dateSelected' },
    { title: 'Status', key: 'status' },
    { title: 'Coordinador', key: 'groundCoordinator' },
  ],
  mappingFn: param => {
    const dates = formatDates(param, ['dateSelected'])
    return {
      ...param,
      ...dates,
    }
  },
}
