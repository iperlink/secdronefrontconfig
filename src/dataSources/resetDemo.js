export const resetDemoList = {
  mainData: {
    name: 'resetDemo',
    fields: [
      'name',
      '_id'
    ],
  },
  title: 'Reset',

  head: [
    { title: 'name', key: 'name' },
  ],

}
