import gql from 'graphql-tag'

const renderUrl = data => {
  const { url } = data
  if (url) {
    const parsed = JSON.parse(url)
    const link = `https://security-drone.wistia.com/medias/${parsed.link}`
    return link
  }
  return ''
}

export const videoEditList = {
  mainData: {
    name: 'videos',
    fields: [
      '_id',
      'date',
      'name',
      'time',
      'objective',
      'row',
      'url',
      'tags',
    ],
    params: ['flight_id'],
  },
  querys: [
    { Q: 'flights', as: 'flights' },
  ],
  title: 'videoTags',
  head: [
    { title: 'nombre', key: 'name' },
    { title: 'Tags', key: 'tags' },
    { title: 'Url', key: 'url', custom: '_link', size: 20 },
    { title: 'Editar', key: '_edit' },
  ],
  extras: {

    _editFn: async (id, data, client) => {
      const editVideos = gql`
        mutation editVideos(
          $_id: String!,
          $tags: String!,
          $name: String!,
          $row: String!,
          $url: String!,
          $flight_id: String!,
          ) {
          editVideos(
            _id: $_id,
            tags:$tags,
            url:$url,
            row:$row,
            name: $name,
            flight_id: $flight_id
            ) {
            _id
          }
        }`
      const variables = {
        ...data,
        _id: id,
      }
      const res = await client.mutate({
        mutation: editVideos,
        variables
      })
      alert('editado con exito')
      location.reload()
    },
    editParams: [
      { name: 'tags', label: 'tags' },
      { name: 'name', label: 'nombre' },
      { name: 'flight_id', label: 'vuelo', select: 'flights' },
      { name: 'row', label: 'categoría' },
      { name: 'url', label: 'url' },
    ],
  },
  mappingFn: param => {
    const url = renderUrl(param)
    return {
      ...param,
      url,
    }
  },
}
