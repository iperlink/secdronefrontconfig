export const planningTemplateList = {
  mainData: {
    name: 'planningTemplates',
    fields: [
      '_id',
      'area',
      'aprovedBy',
      'createdBy',
      'frecuency',
      'groundCoordinator',
      'objective',
      'name',
      'status',
    ],
  },
  title: 'Misiones',
  head: [
    { title: 'Objetivo', key: 'objective', size: 130 },
    { title: 'Area', key: 'area' },
    { title: 'Frecuencia', key: 'frecuency' },
    { title: 'Coordinador', key: 'groundCoordinator' },
    { title: 'nombre', key: 'name' },
  ],
}
