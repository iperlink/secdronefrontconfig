import gql from 'graphql-tag'
export const groupEditList = {
  mainData: {
    name: 'groups',
    fields: ['name', 'privileges', 'contract', 'company', '_id']
  },
  title: 'grupos',
  querys: [{ Q: 'contracts', as: 'contracts' }],
  head: [
    { title: 'Nombre', key: 'name', size: 50 },
    { title: 'Contrato', key: 'contract', size: 60 },
    { title: 'Privilegios', key: 'privileges', size: 180 },
    { title: 'Editar', key: '_edit' }
  ],
  extras: {
    _editFn: async (_id, data, client) => {
      const editGroup = gql`
        mutation editGroup(
          $_id: String!
          $name: String!
          $privileges: String!
          $contract: String!
        ) {
          editGroup(_id: $_id, name: $name, privileges: $privileges, contract: $contract) {
            _id
          }
        }
      `
      const variables = {
        ...data,
        _id
      }
      const res = await client.mutate({
        mutation: editGroup,
        variables
      })
      alert('editado con exito')
      location.reload()
    },
    editParams: [
      { name: 'name', label: 'nombre' },
      { name: 'privileges', label: 'privilegios' },
      { name: 'contract', label: 'contrato' }
    ]
  }
}
