

export const contractList = {
  mainData: {
    name: 'contracts',
    fields: [
      'name',
      '_id',
      'company'
    ],
  },
  title: 'contratos',
  aditionalQuerys: [
    { Q: 'setSelectedNameMutation', as: 'switchEfect' },
  ],
  head: [
    { title: 'Nombre', key: 'name' },
    { title: 'Empresa', key: 'company' },
  ],
}