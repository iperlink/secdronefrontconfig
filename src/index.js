import QUERYS from './querys'
import { routeArr } from './routes'
import { dataSources } from './dataSources'

import { mutationsArray } from './mutations'

import { flexArray } from './flex'
import { registerForms } from './register'


const formArray = {
  ...mutationsArray,
  ...registerForms,
}

const moduleConfig = {
  dataSources,
  formArray,
  QUERYS,
  routeArr,
  flexArray,
  registerForms,
}
exports.moduleConfig = moduleConfig