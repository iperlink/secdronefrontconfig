import { createCompanyForm } from './createCompany.form'
import { createContractForm } from './createContract.form'
import { createGroupForm } from './createGroup.form'
import { createUserForm } from './createUser.form'
import { loginForm } from './login.form'
import { changePasswordForm } from './changePassword.form'


export const registerForms = {
  createCompanyForm,
  createContractForm,
  createGroupForm,
  createUserForm,
  loginForm,
  changePasswordForm,
}
