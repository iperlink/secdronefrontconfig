export const changePasswordForm = {
  file: 'changePassword.form.js',
  omitMutation: true,
  mainMut: 'login',
  fields: [
    {
      name: 'password',
      type: 'text',
      config: {
        placeholder: 'Ingrese password actuall...',
        label: 'Password Actual',
      },
      onChange: null,
    },
    {
      name: 'newPassword',
      type: 'text',
      config: {
        placeholder: 'Ingrese nueva password...',
        label: 'Password',
      },
      onChange: null,
    },
    {
      name: 'newPassword2',
      type: 'text',
      config: {
        placeholder: 'Confirme nueva password...',
        label: 'Confirme Password',
      },
      onChange: null,
    },
  ],
  registerFn: 'changePassword',
}
