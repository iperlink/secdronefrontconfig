
export const createCompanyForm = {
  mainMut: 'createCompany',
  file: 'createCompany.form.js',
  title: 'Crear empresa',
  updateQuerys: ['companies'],
  fields: [
    {
      name: 'name',
      type: 'text',
      config: {
        placeholder: 'Ingrese nombre de empresa...',
        label: 'Nombre',
      },
      onChange: null,
    },
  ],
  create: true,
}
