export const createUserForm = {
  file: 'createUser.form.js',
  mainMut: 'createUser',
  title: 'Crear usuario',
  updateQuerys: ['users'],
  fields: [
    {
      name: 'email',
      type: 'text',
      config: {
        placeholder: 'Ingrese email del usuario...',
        label: 'Email',
      },
    },
    {
      name: 'name',
      type: 'text',
      config: {
        placeholder: 'Ingrese nombre del usuario...',
        label: 'Nombre',
      },
    },
    {
      name: 'lastName',
      type: 'text',
      config: {
        placeholder: 'Ingrese apellido del usuario...',
        label: 'Apellido',
      },
    },
    {
      name: 'phone',
      type: 'text',
      config: {
        placeholder: 'Ingrese telefono del usuario...',
        label: 'Telefono',
      },
    },
    {
      name: 'position',
      type: 'text',
      config: {
        placeholder: 'Ingrese cargo del usuario...',
        label: 'Cargo',
      },
    },
    {
      type: 'select',
      name: 'group',
      config: {
        multi: false,
        label: 'grupo',
        dataFrom: {
          Q: 'groups',
          value: '_id',
          label: 'name',
        },
      },
    },

  ],
  registerFn: 'register',
}
