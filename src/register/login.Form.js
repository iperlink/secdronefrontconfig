export const loginForm = {
  file: 'login.form.js',
  title: 'login',
  mainMut: 'login',
  id: 'login',
  logo: true,
  className: 'loginForm',
  submitBtnTxt: 'Entrar',
  submitBtnMsg: 'false',
  message: 'Bienvenido',
  fields: [
    {
      name: 'email',
      type: 'text',
      config: {
        placeholder: 'Ingrese nombre de email...',
        label: 'Email',
        id: 'email',
      },
      onChange: null,
    },
    {
      name: 'password',
      type: 'password',
      config: {
        placeholder: 'Ingrese password...',
        label: 'Pass',
        id: 'password',
      },
      onChange: null,
    },
    {
      name: 'changePassword',
      type: 'btn',
      config: {
        placeholder: 'Ingrese password...',
        label: 'cambiar password',
      },
      onClick: () => {
        console.log('pressed')
      },
    },
  ],
  omitMutation: true,
}
