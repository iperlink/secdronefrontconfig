
export const createContractForm = {
  mainMut: 'createContract',
  file: 'createContract.form.js',
  title: 'Crear contrato',
  updateQuerys: ['contracts'],
  fields: [
    {
      name: 'name',
      type: 'text',
      config: {
        placeholder: 'Ingrese nombre de contrato...',
        label: 'Nombre',
      },
      onChange: null,
    },
    {
      type: 'select',
      name: 'company',
      config: {
        multi: false,
        label: 'empresa',
        dataFrom: {
          Q: 'companies',
          value: '_id',
          label: 'name',
        },
      },
    },
  ],
  create: true,
}
