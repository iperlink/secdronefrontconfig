import { routeArr } from '../routes'
import _ from 'lodash'
const getPrivileges = () => {
  const privileges = _.map(routeArr.childs, child => {
    const result = child.privileges
    if (!result) {
      console.error(child.path, ': has no privileges defined')
    }
    return result
  })

  const unique = _.without(_.uniq(_.flatten(privileges)), ['crear empresa', 'crear contrato', 'crear grupo'])
  const vals = _.map(unique, u => {
    const result = {
      value: u,
      label: u,
    }
    return result
  })
  return vals
}

export const createGroupForm = {
  mainMut: 'createGroup',
  file: 'createGroup.form.js',
  title: 'Crear grupo',
  updateQuerys: ['groups'],
  fields: [
    {
      name: 'name',
      type: 'text',
      config: {
        placeholder: 'Ingrese nombre de usuario...',
        label: 'Nombre',
      },
    },
    {
      type: 'select',
      name: 'contract',
      config: {
        label: 'contrato',
        dataFrom: {
          Q: 'contracts',
          value: '_id',
          label: 'name',
        },
      },
    },
    {
      type: 'checkbox',
      name: 'privileges',
      config: {
        label: 'privilegios',
        options: getPrivileges()
      },
    },
  ],
  create: true,
}
