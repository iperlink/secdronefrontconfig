const LocalState = {
  mainData: {
    name: 'localState',
    fields: [
      'selectedName @client',
    ],
    local: true,
  },
}
export default LocalState
