const path = require('path')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack')


module.exports = {
  entry: {
      app: './src/ext.js',
    },

  output: {
    filename: 'client_config.js',
    path: path.resolve(__dirname, 'dist'),
    library: 'client_config',
  },
  externals: {

  },
  module: {
  }
}

